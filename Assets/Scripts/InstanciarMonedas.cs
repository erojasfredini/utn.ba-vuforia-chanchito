﻿//MIT License
//
//Copyright(c) 2019 Emmanuel Rojas Fredini
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
using UnityEngine;

public class InstanciarMonedas : MonoBehaviour
{
    public GameObject moneda;
    public float maxRadioCoef = 1.5f;
    public float minRadioCoef = 1.1f;
    public uint cantidadMonedas = 15;
    public float y = 0.0f;
    public float velocidad = 1.0f;

    private Collider volumenObjeto;
    private Vector3 centro;
    private float radio;

    void Awake()
    {
        volumenObjeto = GetComponent<Collider>();

        centro = volumenObjeto.bounds.center;
        radio = volumenObjeto.bounds.extents.magnitude;

        // Padre
        GameObject emptyParent = new GameObject("Monedas");
        emptyParent.transform.parent = transform;
        emptyParent.transform.position = centro;
        var rotate = emptyParent.AddComponent<Rotate>();
        rotate.velocidad = velocidad;
        rotate.ejeRotacion = Rotate.eje.ParentUp;

        // Monedas
        for (uint i = 0; i < cantidadMonedas; ++i)
        {
            //var delta = Random.insideUnitCircle.normalized * radio * Mathf.Lerp(minRadioCoef, maxRadioCoef, Random.Range(0.0f,1.0f));
            var deltaAng = (Mathf.PI * 2.0f) / cantidadMonedas;
            var delta = new Vector2(Mathf.Cos(deltaAng * i), Mathf.Sin(deltaAng * i));
            delta *= radio * Mathf.Lerp(minRadioCoef, maxRadioCoef, Random.Range(0.0f, 1.0f));
            Quaternion rot = Quaternion.identity;
            rot.eulerAngles = Random.insideUnitSphere * 360.0f;
            GameObject.Instantiate(moneda, centro + new Vector3(delta.x, y, delta.y), rot, emptyParent.transform);
        }
    }

    private void OnDrawGizmosSelected()
    {
        volumenObjeto = GetComponent<Collider>();

        var centro = volumenObjeto.bounds.center;
        var radio = volumenObjeto.bounds.extents.magnitude;

        // Altura dde las monedas
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(centro + Vector3.up * y, new Vector3(radio, radio * 0.1f, radio));

        // Radios minimo y maximo
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(centro + Vector3.up * y, radio * minRadioCoef);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(centro + Vector3.up * y, radio * maxRadioCoef);
    }
}
